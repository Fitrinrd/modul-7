import java.awt.Color;
public class Kucing{
	private String nama;
	private Color warnaBulu;
	private int usia;
	private double bb;
	private boolean statusJinak;
	private String majikan;

	private void cetakInformasi(){
		System.out.println("Kucing Bernama : "+nama);
		System.out.println("Warna Bulu : "+warnaBulu);
		System.out.println("Usia : "+usia);
		System.out.println("Berat Badan : "+bb);
		System.out.println("Jinak ? : "+apakahJinak());
		System.out.println("Diadopsi oleh : "+majikan);
	}
	private void diadopsi(String m){
		majikan = m;
		statusJinak = true;
	}
	private boolean apakahJinak(){
		return statusJinak;
	}
	private void dilepas(){
		majikan = " ";
		statusJinak = false;
	}
}